#!/bin/bash

set -eu

echo "=> Ensure directories"
mkdir -p /app/data/addons  /app/data/data

if [[ ! -f "/app/data/odoo.conf" ]]; then
    echo "=> First run, create config file"
    cp /etc/odoo/odoo.conf /app/data/odoo.conf
fi

echo "=> Patch config file"
# https://github.com/odoo/docker/blob/master/10.0/odoo.conf
crudini --set /app/data/odoo.conf options addons_path /app/data/addons,/usr/lib/python2.7/dist-packages/odoo/addons
crudini --set /app/data/odoo.conf options data_dir /app/data/data
crudini --set /app/data/odoo.conf options db_host ${POSTGRESQL_HOST}
crudini --set /app/data/odoo.conf options db_port ${POSTGRESQL_PORT}
crudini --set /app/data/odoo.conf options db_user ${POSTGRESQL_USERNAME}
crudini --set /app/data/odoo.conf options db_password ${POSTGRESQL_PASSWORD}
crudini --set /app/data/odoo.conf options db_dbname ${POSTGRESQL_DATABASE}
crudini --set /app/data/odoo.conf options smtp_password ${MAIL_SMTP_PASSWORD}
crudini --set /app/data/odoo.conf options smtp_port ${MAIL_SMTP_PORT}
crudini --set /app/data/odoo.conf options smtp_server ${MAIL_SMTP_SERVER}
crudini --set /app/data/odoo.conf options smtp_user ${MAIL_SMTP_USERNAME}
crudini --set /app/data/odoo.conf options smtp_ssl False
crudini --set /app/data/odoo.conf options email_from ${MAIL_FROM}
crudini --set /app/data/odoo.conf options list_db False

echo "=> Ensure data ownership"
chown -R odoo:odoo /app/data/

echo "=> Starting odoo"
exec /usr/local/bin/gosu odoo:odoo /usr/bin/odoo --config=/app/data/odoo.conf
