FROM cloudron/base:0.10.0
MAINTAINER Johannes Zellner <support@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

RUN wget -O - https://nightly.odoo.com/odoo.key | apt-key add -
RUN echo "deb http://nightly.odoo.com/10.0/nightly/deb/ ./" >> /etc/apt/sources.list.d/odoo.list
RUN apt-get update && apt-get -y install odoo wkhtmltopdf && rm -r /var/cache/apt /var/lib/apt/lists

# patch to accept a db name
COPY sql_db.py /usr/lib/python2.7/dist-packages/odoo/sql_db.py

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
